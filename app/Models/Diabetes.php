<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Diabetes extends Model
{
    use HasFactory;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'waktu',
        'jenisPemeriksaan',
        'nilai',
        'catatan',
        'oleh',
        'idUser',
        'created_at'
    ];

    

    public function user()
    {
        return $this->hasOne(User::class,'id','idUser');
    }

    public function getCreatedAtAttribute($created_at)
    {
       return Carbon::parse($created_at)
            ->locale('id')->addHours(7)->settings(['formatFunction' => 'translatedFormat'])->format('l, j F Y ; H:i a'); 
    }
    public function getUpdatedAtAttribute($updated_at)
    {
        return Carbon::parse($updated_at)
            ->locale('id')->addHours(7)->settings(['formatFunction' => 'translatedFormat'])->format('l, j F Y ; H:i a');
    }
}
