<?php

namespace App\Http\Controllers;

use App\Models\Diabetes;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DiabetesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function index()
    {
        $diabetes = Diabetes::paginate(10);

        return view('diabetes.index', [
            'diabetes' => $diabetes
        ]);
    }

   
    public function destroy(Diabetes $diabetes)
    {
        $diabetes->delete();

        return redirect()->route('diabetes.index');
    }

    public function cari(Request $request)
	{
		// menangkap data pencarian
		$cari = $request->cari;
 
    		// mengambil data dari table pegawai sesuai pencarian data
		$diabetes = DB::table('diabetes')
		->where('waktu','like',"%".$cari."%")
        ->orwhere('jenisPemeriksaan','like',"%".$cari."%")
		->paginate();
 
    		// mengirim data pegawai ke view index
		return view('diabetes.index',['diabetes' => $diabetes]);
 
	}
}
