<?php

namespace App\Http\Controllers;

use App\Models\Food;
use App\Models\Luka;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LukaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function index()
    {
        $luka = Luka::paginate(10);

        return view('luka.index', [
            'luka' => $luka
        ]);
    }

    

    
    public function destroy(Luka $luka)
    {
        $luka->delete();

        return redirect()->route('luka.index');
    }

    public function cari(Request $request)
	{
		// menangkap data pencarian
		$cari = $request->cari;
 
    		// mengambil data dari table pegawai sesuai pencarian data
		$luka = DB::table('lukas')
		->where('catatan','like',"%".$cari."%")
        ->orwhere('picturePath','like',"%".$cari."%")
		->paginate();
 
    		// mengirim data pegawai ke view index
		return view('luka.index',['luka' => $luka]);
 
	}
}
