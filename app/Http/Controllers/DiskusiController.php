<?php

namespace App\Http\Controllers;

use App\Models\Diskusi;
use App\Models\Food;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class DiskusiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function index()
    {
        $diskusi = Diskusi::paginate(10);

        return view('diskusi.index', [
            'diskusi' => $diskusi
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('diskusi.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $data['idUser']=Auth::user()->id;
        $data['oleh']=Auth::user()->name;

        Diskusi::create($data);

        return redirect()->route('diskusi.index');
    }

    
    public function destroy(Diskusi $diskusi)
    {
        $diskusi->delete();

        return redirect()->route('diskusi.index');
    }

    public function cari(Request $request)
	{
		// menangkap data pencarian
		$cari = $request->cari;
 
    		// mengambil data dari table pegawai sesuai pencarian data
		$diskusi = DB::table('diskusis')
		->where('diskusi','like',"%".$cari."%")
		->paginate();
 
    		// mengirim data pegawai ke view index
		return view('diskusi.index',['diskusi' => $diskusi]);
 
	}
}
