<?php

namespace App\Http\Controllers\API;

use App\Helpers\ResponseFormatter;
use App\Http\Controllers\Controller;
use App\Models\Diabetes;
use App\Models\Food;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DiabetesController extends Controller
{
    public function all(Request $request)
    {
        $id = $request->input('id');
        $limit = $request->input('limit', 1000);
        $idUser = $request->input('idUser');
       

        /* if($id)
        {
            $diabetes = Diabetes::find($id);

            if($diabetes)
                return ResponseFormatter::success(
                    $diabetes,
                    'Data berhasil diambil'
                );
            else
                return ResponseFormatter::error(
                    null,
                    'Data tidak ditemukan',
                    404
                );
        } */

        $diabetes = Diabetes::query()->orderBy('id', 'desc');

        if($idUser)
            $diabetes->where('idUser', 'like', '%' . $idUser . '%');

        

        return ResponseFormatter::success(
            $diabetes->paginate($limit),
            'Data list diabetes berhasil diam'
        );
    }

    public function simpanDiabetes(Request $request)
    {
        
        try {
            $request->validate([
                'waktu'=>'required',
                'jenisPemeriksaan'=>'required',
                'nilai'=>'required',
                'oleh'=>'required',
                'idUser'=>'required',
                'created_at'=>'required'
            ]);
            Diabetes::create([
                'waktu'=>$request->waktu,
                'jenisPemeriksaan'=>$request->jenisPemeriksaan,
                'nilai'=>$request->nilai,
                'catatan'=>$request->catatan,
                'oleh'=>$request->oleh,
                'idUser'=>$request->idUser,
                'created_at'=>$request->created_at
            ]);

            return response()->json(['status'=>'200','success'=>'Data berhasil dimasukan']);


        } catch (Exception $error) {
            return response()->json(['status'=>'201','error'=>$error->getMessage()]);
        }

    }

    public function updateDiabetes(Request $request,$id)
    {
        try {
            $request->validate([
                'waktu'=>'required',
                'jenisPemeriksaan'=>'required',
                'nilai'=>'required',
            ]);

            Diabetes::updateOrCreate(
                ['id'=>$id],
                [
                    'waktu'=>$request->waktu,
                    'jenisPemeriksaan'=>$request->jenisPemeriksaan,
                    'nilai'=>$request->nilai,
                    'catatan'=>$request->catatan,
                    'oleh'=>'adminUpdate',
                    'idUser'=>'1'
                ]);
            
            return response()->json(['status'=>'200','success'=>'Data berhasil diupdate']);

        } catch (Exception $error) {
            return response()->json(['status'=>'201','error'=>$error->getMessage()]);
        }
    }

    public function deleteDiabetes($id)
    {
        try {
            Diabetes::find($id)->delete();
            return response()->json(['status'=>'200','success'=>'Data Diabetes dengan id='.$id.' Berhasil Dihapus !']);

        } catch (Exception $error) {
            return response()->json(['status'=>'201','error'=>$error->getMessage()]);
        }
    }


    public function hasilDiabetes(Request $request,$id)
    {
        try {
            if($id)
            {
           
            $genNilai=ResponseFormatter::generateHasil(ResponseFormatter::cekNilai('Gula Darah Sewaktu'),ResponseFormatter::cekNilai('Gula Darah Puasa'),ResponseFormatter::cekNilai('GD 2 Jam Setelah Makan'),ResponseFormatter::cekNilai('HbA1c'));
                        $diabetes=[];
                        
                            $diabetes[]=[
                                'GulaDarahS'=>ResponseFormatter::cekNilai('Gula Darah Sewaktu'),
                                'GulaDarahP'=>ResponseFormatter::cekNilai('Gula Darah Puasa'),
                                'GulaDarahDua'=>ResponseFormatter::cekNilai('GD 2 Jam Setelah Makan'),
                                'HB'=>ResponseFormatter::cekNilai('HbA1c'),
                                'hasil'=>$genNilai
                            ];
        

            if($diabetes)
                return ResponseFormatter::success(
                    $diabetes,
                    'Data berhasil diambil'
                );
            else
                return ResponseFormatter::error(
                    null,
                    'Data tidak ditemukan',
                    404
                );
            }
        } catch (Exception $error) {
            return response()->json(['status'=>'201','error'=>$error->getMessage()]);
        }        
    }
}
