<?php

namespace App\Http\Controllers\API;

use App\Helpers\ResponseFormatter;
use App\Http\Controllers\Controller;
use App\Models\Diskusi;
use App\Models\MasterQuiz;
use App\Models\Quiz;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DiskusiController extends Controller
{
    public function all(Request $request)
    {
        $id = $request->input('id');
        $limit = $request->input('limit', 1000);
        
       

        if($id)
        {
            $diabetes = Diskusi::find($id);

            if($diabetes)
                return ResponseFormatter::success(
                    $diabetes,
                    'Data berhasil diambil'
                );
            else
                return ResponseFormatter::error(
                    null,
                    'Data tidak ditemukan',
                    404
                );
        }

        $diabetes = Diskusi::query();

        

        

        return ResponseFormatter::success(
            $diabetes->paginate($limit),
            'Data list diabetes berhasil diambil'
        );
    }

    public function simpanDiskusi(Request $request)
    {
        try {
            $request->validate([
                'diskusi'=>'required',
            ]);

            Diskusi::create([
                'diskusi'=>$request->diskusi,
                'oleh'=>ResponseFormatter::getUser($request->idUser),
                'idUser'=>$request->idUser
            ]);

            return response()->json(['status'=>'200','success'=>'Data berhasil dimasukan']);


        } catch (Exception $error) {
            return response()->json(['status'=>'201','error'=>$error->getMessage()]);
        }

    }

    public function soal(Request $request)
    {
            $masterquiz = MasterQuiz::select(DB::raw('
                            pertanyaankuis,
                            jawaban
                            '))->get();
            return ResponseFormatter::success(
                    $masterquiz,
                    'Data berhasil diambil'
                );
        
        
    }

    public function simpanSoal(Request $request)
    {
        try {
            $request->validate([
                'pertanyaankuis'=>'required',
                'jawaban'=>'required',
                'jawab'=>'required',
            ]);

            Quiz::create([
                'pertanyaankuis'=>$request->pertanyaankuis,
                'jawaban'=>$request->jawaban,
                'jawab'=>$request->jawab,
                'idLuka'=>'1',
                'oleh'=>'admin',
                'idUser'=>'1'
            ]);

            return response()->json(['status'=>'200','success'=>'Data berhasil dimasukan']);


        } catch (Exception $error) {
            return response()->json(['status'=>'201','error'=>$error->getMessage()]);
        }

    }
}
