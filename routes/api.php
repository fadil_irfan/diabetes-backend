<?php

use App\Http\Controllers\API\DiabetesController;
use App\Http\Controllers\API\LukaController;
use App\Http\Controllers\API\DiskusiController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\FoodController;
use App\Http\Controllers\API\MidtransController;
use App\Http\Controllers\API\UserController;
use App\Http\Controllers\API\TransactionController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/versi', function () {
    return 'Rest API Aplikasi Deka 1.0';
});


Route::middleware(['auth:sanctum'])->group(function () {
    Route::get('user', [UserController::class, 'fetch']);
    Route::post('user', [UserController::class, 'updateProfile']);
    Route::post('user/photo', [UserController::class, 'updatePhoto']);
    Route::get('transaction', [TransactionController::class, 'all']);
    Route::post('transaction/{id}', [TransactionController::class, 'update']);
    Route::post('checkout', [TransactionController::class, 'checkout']);
    Route::post('logout', [UserController::class, 'logout']);
});

Route::post('login', [UserController::class, 'login']);
Route::post('register', [UserController::class, 'register']);

Route::get('food', [FoodController::class, 'all']);
Route::post('midtrans/callback', [MidtransController::class, 'callback']);


Route::get('diabetes/{id}', [DiabetesController::class, 'all']);
Route::post('simpanDiabetes', [DiabetesController::class, 'simpanDiabetes']);
Route::put('updateDiabetes', [DiabetesController::class, 'updateDiabetes']);
Route::delete('deleteDiabetes', [DiabetesController::class, 'deleteDiabetes']);
Route::get('hasilDiabetes/{id}', [DiabetesController::class, 'hasilDiabetes']);

Route::get('luka/{id}', [LukaController::class, 'all']);
Route::post('simpanLuka', [LukaController::class, 'simpanLuka']);
Route::put('updateLuka', [LukaController::class, 'updateLuka']);
Route::delete('deleteLuka', [LukaController::class, 'deleteLuka']);

Route::get('diskusi/{id}', [DiskusiController::class, 'all']);
Route::post('simpanDiskusi', [DiskusiController::class, 'simpanDiskusi']);

Route::get('soal/{id}', [DiskusiController::class, 'soal']);
Route::post('simpanSoal', [DiskusiController::class, 'simpanSoal']);

