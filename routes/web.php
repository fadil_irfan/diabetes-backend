<?php

use App\Http\Controllers\API\MidtransController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\FoodController;
use App\Http\Controllers\TransactionController;
use App\Http\Controllers\DiabetesController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\LukaController;
use App\Http\Controllers\DiskusiController;
use App\Models\Diabetes;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Homepage
Route::get('/', function () {
    return redirect()->route('admin-dashboard');
});



// Dashboard
Route::prefix('dashboard')
    ->middleware(['admin'])
    ->group(function() {
        Route::get('/', [DashboardController::class, 'index'])->name('admin-dashboard');
       
        Route::resource('users', UserController::class);
        Route::resource('diabetes', DiabetesController::class);
        Route::resource('luka', LukaController::class);
        Route::resource('diskusi', DiskusiController::class);
        Route::post('diabetes/cari', [DiabetesController::class, 'cari'])->name('diabetes.cari');
        Route::post('luka/cari', [LukaController::class, 'cari'])->name('luka.cari');
        Route::post('diskusi/cari', [DiskusiController::class, 'cari'])->name('diskusi.cari');
        

        
    });

