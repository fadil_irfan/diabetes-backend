<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Diabetes') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="mb-10">
               <form class="w-full" action="{{ route('diabetes.cari') }}" method="post" >
                    @csrf
                    <div class="flex flex-wrap -mx-3 mb-6">
                        <div class="w-full px-3">
                            <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-last-name">
                                Pencarian
                            </label>
                            <input value="{{ old('cari') }}" name="cari" class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="grid-last-name" type="text" placeholder="kata Pencarian">
                        </div>
                    </div>
                    
                    
                    
                    
                    
                    <div class="flex flex-wrap -mx-3 mb-6">
                        <div class="w-full px-3 text-right">
                            <button type="submit" class="bg-green-500 hover:bg-green-700 text-white font-bold py-2 px-4 rounded">
                                Cari
                            </button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="bg-white">
                <table class="table-auto w-full">
                    <thead>
                    <tr>
                        <th class="border px-6 py-4">No</th>
                        <th class="border px-6 py-4">Nama</th>
                        <th class="border px-6 py-4">Waktu</th>
                        <th class="border px-6 py-4">Jenis Pemeriksaan</th>
                        <th class="border px-6 py-4">Nilai</th>
                        <th class="border px-6 py-4">Catatan</th>
                        <th class="border px-6 py-4">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                        @forelse($diabetes as $item)
                            <tr>
                                <td class="border px-6 py-4" align="center">{{ $loop->iteration }}</td>
                                <td class="border px-6 py-4 ">{{ App\Helpers\ResponseFormatter::cekNamaUser($item->idUser) }} </td>
                                <td class="border px-6 py-4">{{ $item->waktu }}</td>
                                <td class="border px-6 py-4">{{ $item->jenisPemeriksaan }}</td>

                                <td class="border px-6 py-4">{{ number_format($item->nilai,2) }}</td>
                                
                                <td class="border px-6 py-4">{{ $item->catatan }}</td>
                                <td class="border px-6 py- text-center">
                                    
                                    <form action="{{ route('diabetes.destroy', $item->id) }}" method="POST" class="inline-block">
                                        {!! method_field('delete') . csrf_field() !!}
                                        <button type="submit" onclick="return confirm('Yakin Hapus Data ?')"  class="bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-4 mx-2 rounded inline-block">
                                            Hapus
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        @empty
                            <tr>
                               <td colspan="7" class="border text-center p-5">
                                   Data Tidak Ditemukan
                               </td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
            <div class="text-center mt-5">
                {{ $diabetes->links() }}
            </div>
        </div>
    </div>
</x-app-layout>
